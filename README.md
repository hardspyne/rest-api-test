# Rest Api Test
____
***automation tests to check status and response***

#### Requests:
 1. GET/delay/{delay} Returns a delayed response (max of 10 seconds).
 2. GET/image/png Returns a simple PNG image.
### Run requirements:
 - JDK 8
 - Maven
 
### How to run:
 1. Open command line in the project directory and enter 'mvn test'
 