import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.*;


import static io.restassured.RestAssured.given;

public class RestApiTest {

    private static ByteArrayOutputStream expectedFileByteArrayOut = new ByteArrayOutputStream();

    @BeforeClass
    public static void downloadExpectedImageBytes() {
            downloadImage(new BufferedInputStream(
                    RestApiTest.class.getResourceAsStream("/expectedImage.webp"))
                    , expectedFileByteArrayOut);
    }

    @Before
    public void setup() {
        RestAssured.baseURI = "https://httpbin.org";
    }

    @Test
    public void getDelayedResponse() {
        given().when().get("/delay/1").then()
                .contentType(ContentType.JSON)
                .body("data", Matchers.isEmptyString())
                .body("headers.Host", Matchers.equalTo("httpbin.org"))
                .body("url", Matchers.equalTo("https://httpbin.org/delay/1"))
                .time(Matchers.lessThan(10000L))
                .statusCode(200);
    }

    @Test
    public void getImage() {
        Response response = given()
                .accept("image/webp").when()
                .get("/image");

        ByteArrayOutputStream actualFileByteArrayOut = new ByteArrayOutputStream();
        downloadImage(response.getBody().asInputStream(), actualFileByteArrayOut);

        //check the image are same
        Assert.assertArrayEquals(actualFileByteArrayOut.toByteArray(), expectedFileByteArrayOut.toByteArray());

        response.then().contentType("image/webp")
                .statusCode(200);
    }

    private static void downloadImage(InputStream in, OutputStream out) {
        try {
            for (int i; (i = in.read()) != -1; ) {
                out.write(i);
            }

            in.close();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
